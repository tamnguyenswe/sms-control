package com.example.smscontrol.helpers

import kotlinx.coroutines.suspendCancellableCoroutine
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

// Based on https://github.com/gildor/kotlin-coroutines-okhttp
suspend fun Call.await(assertSuccess : Boolean = false) : Response {
    return suspendCancellableCoroutine { continuation ->
        enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                if (continuation.isCancelled)
                    return
                continuation.resumeWithException(e)
            }

            override fun onResponse(call: Call, response: Response) {
                if (assertSuccess && !response.isSuccessful) {
                    continuation.resumeWithException(Exception("HTTP error ${response.code}"))
                    return
                }
                continuation.resume(response)
            }

        })

        continuation.invokeOnCancellation {
            try {
                cancel()
            } catch (exception: Throwable) {
                // Do nothing
            }
        }
    }
}