package com.example.smscontrol.repositories

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.smscontrol.assets.Constants
import com.example.smscontrol.assets.SMS
import com.example.smscontrol.assets.SMS.Status.*
import com.example.smscontrol.helpers.JavaMailAPI
import java.io.File
import java.io.FileWriter
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

private const val TAG = "SmsRepository"

object SmsRepository {
    var mSmsList : MutableLiveData<LinkedList<SMS>> = MutableLiveData(LinkedList())
        private set

    lateinit var mLogFile : File
        private set

    lateinit var mLogFilePath : File
        private set

    private var mIsInitialized = false


    /**
     * Update SMS List. Use [context] to initialize if not yet done.
     */
    fun updateSmsList(context: Context) {
        if (!mIsInitialized)
            init(context)

        val dataTable : Uri = Uri.parse("content://sms/")
        val cursor : Cursor = context.contentResolver.query(
            dataTable,
            null,
            null,
            null,
            null
        )!!

        val checkedIds: ArrayList<String> = getCheckedIDs()

        cursor.moveToFirst()
        val smsList = LinkedList<SMS>()

        
        while (!cursor.isAfterLast) {
            val address: String = cursor.getString(Constants.SMS.ADDRESS)
            val timeReceived: String = cursor.getString(Constants.SMS.TIME_RECEIVED)
            val timeSent: String = cursor.getString(Constants.SMS.TIME_SENT)
            val body: String = cursor.getString(Constants.SMS.BODY)

            val sms = SMS(address, timeReceived, timeSent, body)

            if (timeReceived.toLong() > Constants.TIME_STAMP)  // all sms received after timestamp
                if (sms.ID in checkedIds) {// and not processed yet
                    sms.status = Checked
                } else {
                    sms.status = Unchecked
                    forward(sms)
                }
            else
                sms.status = NotRelevant

            smsList.add(sms)
            cursor.moveToNext()
        }

        cursor.close()
        mSmsList.value = smsList //set value to live data to trigger observer
    }


    /**
     * Read all [SMS.ID]s marked as [Checked] storing in device's internal storage
     */
    private fun getCheckedIDs() : ArrayList<String> {
        val checkedIDs = ArrayList<String>()

        for (line in mLogFile.readLines()) {
            checkedIDs.add(line)
        }

        return checkedIDs
    }


    /**
     * Initializing with an application [Context] to get internal file paths then throw the context away to avoid memory leaks.
     */
    fun init(context: Context) {
        mSmsList.value = LinkedList()

        mLogFilePath = context.filesDir
        mLogFile = File(mLogFilePath, Constants.LOG_FILE_NAME)

        if (!mLogFile.exists())
            mLogFile.createNewFile()

        mLogFile.setWritable(true)

        mIsInitialized = true
    }


    fun onReceiveNewSms(newSMS : SMS) {
        forward(newSMS) // forward this sms
        val smsList : LinkedList<SMS> = mSmsList.value!!
        smsList.addFirst(newSMS) // insert this sms into most left position

        mSmsList.value = smsList
    }


    /**
     * Forward an [Unchecked] [SMS] to my email
     */
    private fun forward(sms : SMS) {
        // only process unchecked sms
        if (sms.status != Unchecked)
            return

        val datetime = SimpleDateFormat("HH:mm, dd/MM/yy").format(sms.datetime()).toString()

        val emailSubject = "New SMS from ${sms.ADDRESS}"
        val emailBody = "${sms.ADDRESS} at $datetime:\n\n${sms.BODY}"
        val mailAPI = JavaMailAPI(Constants.EMAIL_DESTINATION, emailSubject, emailBody, sms)
        mailAPI.execute()
    }


    /**
     * Mark a [SMS] as [Checked]. Only use after forwarding that sms.
     */
    fun markSMS(markingSms : SMS) {
//        sms.status = Checked

        val writer = FileWriter(mLogFile, true)
        writer.write("${markingSms.ID}\n")
        writer.flush()
        writer.close()

        Log.d(TAG, "Marked ${markingSms.ID} as checked")

        // find sms in sms list and mark it as checked
        val smsList : LinkedList<SMS> = mSmsList.value!!
        for (sms : SMS in smsList) {
            if (sms.ID == markingSms.ID)
                sms.status = Checked
        }

        mSmsList.value = smsList
    }

}