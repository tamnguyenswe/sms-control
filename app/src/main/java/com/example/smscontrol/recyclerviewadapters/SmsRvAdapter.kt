package com.example.smscontrol.recyclerviewadapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.smscontrol.R
import com.example.smscontrol.assets.Constants
import com.example.smscontrol.assets.SMS
import java.text.SimpleDateFormat
import java.util.LinkedList

private const val TAG = "SmsRvAdapter"
class SmsRvAdapter(smsList: LinkedList<SMS>)
                : RecyclerView.Adapter<SmsRvAdapter.ViewHolder>() {

    private var mSmsList = smsList

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        val ibTick : ImageButton = itemView.findViewById(R.id.ib_button)
        val tvNumber : TextView = itemView.findViewById(R.id.tv_sender_number)
        val tvBody : TextView = itemView.findViewById(R.id.tv_sms_body)
        val tvDatetime : TextView = itemView.findViewById(R.id.tv_date_time)
        val layout : ConstraintLayout = itemView.findViewById(R.id.layout_single_sms)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.layout_single_sms, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mSmsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sms : SMS = mSmsList[position]

        holder.tvNumber.text = sms.ADDRESS
        holder.tvBody.text = sms.croppedBody()
        holder.tvDatetime.text = SimpleDateFormat("HH:mm, dd/MM/yy").format(sms.datetime()).toString()

        when (sms.status) {
            SMS.Status.Checked -> {
                holder.layout.setBackgroundColor(Constants.Colors.GREEN_LIGHT)
            }

            SMS.Status.Unchecked -> {
                holder.layout.setBackgroundColor(Constants.Colors.ORANGE_LIGHT)
            }

            SMS.Status.NotRelevant -> {
                holder.layout.setBackgroundColor(Color.WHITE)
            }
        }
    }

    fun updateSmsList(smsList: LinkedList<SMS>) {
        this.mSmsList = smsList
    }

}