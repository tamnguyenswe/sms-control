# Description:
 A small mobile app to retrieve the device's SMSs and send it to my email. 
# Background:
 Each time I need to retrieve an OTP code sent to my phone number in Vietnam, I need someone to forward that OTP to me, and that's pretty inconvenient because of timezone differences and other things. 
 With this app each time a new SMS was sent to my phone number, it will automatically be forwarded to my email address. Whoever possessing the cell phone with my phone number can also synchronize the SMSs manually.
# Disclaimer:
 This application should not be used for wrong purposes.
 This application is targeted to run on a LG G3 running android 5.1 ~ 6.0. It may or may not work on other devices and other android versions.
# Screenshots:
![](https://i.imgur.com/1QVn62K.png?1)  

Version 1.0 on Android API 22 (5.1 Lollipop)
       
   
![](https://i.imgur.com/tY6VbvQ.png?1)
  
Received emails
   
    
![](https://i.imgur.com/7k8F8jL.png?1)
  
New emails will be almost instantly pushed to my phone as long as I have an internet connection
